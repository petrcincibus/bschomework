package cz.cincibus.bschomework.presentation;


import cz.cincibus.bschomework.domain.PaymentService;
import cz.cincibus.bschomework.model.PaymentWithRate;

import java.util.Collection;

/**
 * Created by Petr Cincibus on 03.12.15.
 * email: petrcincibus@gmail.com
 */
public class Presenter {

    private static final String QUIT_COMMAND = "quit";

    private View view;

    private InputHelper inputHelper = new InputHelper();

    private PaymentService paymentService = new PaymentService();

    private PrintTimer printTimer;

    public Presenter(View view) {
        this.view = view;
    }

    /**
     * Initialize presenter
     */
    public void initialize(){
        printTimer = new PrintTimer(this);
        printTimer.start();
    }

    /**
     * Function processing user input.
     * @param line insert command
     */
    public void insertedLine(String line){
        if(line.equals(QUIT_COMMAND)) quit();
        if(!inputHelper.isLegal(line)) {
            view.print("Inserted command has a wrong syntax.");
        }
        else {
            inputHelper.loadLine(line);
        }
    }

    /**
     * End of application.
     */
    private void quit() {
        System.exit(0);
    }

    /**
     * Prints payments with exchange rate to view.
     * @param items collection of payments with exchange rate
     */
    public void print(Collection<PaymentWithRate> items){
        for(PaymentWithRate p: items){
            if(p.payment.value == 0) continue;
            view.print(p.toString());
        }
    }

    /**
     * Print timer handler
     */
    public void printNotify() {
        Collection<PaymentWithRate> paymentsWithCurrency = paymentService.getAllPaymentWithRate();
        print(paymentsWithCurrency);
    }


}
