package cz.cincibus.bschomework.model;

/**
 * Created by Petr Cincibus on 08.12.15.
 * email: petrcincibus@gmail.com
 */
public class PaymentWithRate {

    public Payment payment;

    public ExchangeRate exchangeRate;

    public PaymentWithRate(Payment payment, ExchangeRate exchangeRate) {
        this.payment = payment;
        this.exchangeRate = exchangeRate;
    }

    /**
     * Computes exchanged value.
     * @return value in exchange target currency
     */
    private double exchange(){
        return payment.value / exchangeRate.value;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(payment);
        if(exchangeRate != null) {
            sb.append(" (USD ");
            sb.append(exchange());
            sb.append(")");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentWithRate that = (PaymentWithRate) o;

        if (payment != null ? !payment.equals(that.payment) : that.payment != null) return false;
        return !(exchangeRate != null ? !exchangeRate.equals(that.exchangeRate) : that.exchangeRate != null);

    }

    @Override
    public int hashCode() {
        int result = payment != null ? payment.hashCode() : 0;
        result = 31 * result + (exchangeRate != null ? exchangeRate.hashCode() : 0);
        return result;
    }
}
