package cz.cincibus.bschomework.data;

import cz.cincibus.bschomework.model.ExchangeRate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Petr Cincibus on 04.12.15.
 * email: petrcincibus@gmail.com
 */
public class ExchangeRateStore {

    private static ExchangeRateStore instance = new ExchangeRateStore();

    private Map<String, Double> map = new ConcurrentHashMap<>();

    private ExchangeRateStore() {}

    public static ExchangeRateStore getInstance() {
        return instance;
    }

    public void add(ExchangeRate exchangeRate){
        map.put(exchangeRate.name, exchangeRate.value);
    }

    /**
     * Get exchange rate of a currency specified by a name. If exchange rate of the currency is not present
     * the returns a zero. Method is not synchronized because there is no method in ExchangeRateStore class
     * which is able to remove element from the map between containsKey() and get() operation except for clear()
     * which is only for testing purposes.
     * @param name currency name
     * @return ExchangeRate object.
     */
    public ExchangeRate get(String name){
        if(map.containsKey(name)) {
            double v = map.get(name);
            return new ExchangeRate(name, v);
        } else{
            return null;
        }
    }

    /**
     * This method is only used for testing  purposes. If it is used in application then could cause get
     * method having problem with synchronization.
     */
    public void clear(){
        map.clear();
    }
}
