package cz.cincibus.bschomework.data;

import cz.cincibus.bschomework.model.Payment;

import java.util.*;

/**
 * Created by Petr Cincibus on 03.12.15.
 * email: petrcincibus@gmail.com
 */
public class PaymentStore {

    private static final PaymentStore instance = new PaymentStore();

    private Map<String, Integer> map;

    private PaymentStore(){
        map = new HashMap<String, Integer>();
    }

    public static PaymentStore getInstance(){
        return instance;
    }

    public synchronized void add(Payment payment){
        if(map.containsKey(payment.name)){
            int oldValue = map.get(payment.name);
            map.put(payment.name, oldValue + payment.value);
        }
        else {
            map.put(payment.name, payment.value);
        }
    }

    public void add(Collection<Payment> payments){
        payments.stream().forEach(this::add);
    }

    public synchronized Collection<Payment> getAll(){
        List<Payment> paymentList = new ArrayList();
        map.entrySet().stream().forEach((e)->paymentList.add(new Payment(e.getKey(),e.getValue())));
        return paymentList;
    }

    /**
     * This method is only for testing purposes.
     */
    public synchronized void clear(){
        map.clear();
    }
}
