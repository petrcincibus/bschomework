package cz.cincibus.bschomework.domain;

import cz.cincibus.bschomework.data.ExchangeRateStore;
import cz.cincibus.bschomework.model.ExchangeRate;

/**
 * Created by Petr Cincibus on 06.12.15.
 * email: petrcincibus@gmail.com
 */
public class ExchangeRateService {

    public void add(ExchangeRate exchangeRate) {
        ExchangeRateStore.getInstance().add(exchangeRate);
    }
}
