package cz.cincibus.bschomework;

import cz.cincibus.bschomework.presentation.View;

import java.util.function.Consumer;

/**
 * Created by Petr Cincibus on 06.12.15.
 * email: petrcincibus@gmail.com
 */
public class TestView implements View {

    /**
     * Print handler
     */
    private Consumer<String> testFunction;

    public TestView(Consumer<String> testFunction) {
        this.testFunction = testFunction;
    }

    @Override
    public void print(String text) {
        testFunction.accept(text);
    }
}
