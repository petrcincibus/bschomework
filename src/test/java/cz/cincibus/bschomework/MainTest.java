package cz.cincibus.bschomework;

import cz.cincibus.bschomework.data.PaymentStore;
import cz.cincibus.bschomework.model.Payment;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Petr Cincibus on 09.12.15.
 * email: petrcincibus@gmail.com
 */
public class MainTest {

    private static final String USD = "USD";

    private static final String RMB = "RMB";

    private static final String HKD = "HKD";

    PaymentStore paymentStore = PaymentStore.getInstance();

    @Before
    public void initialize(){
        paymentStore.clear();
    }

    @Test
    public void testLoadFile01(){
        Main.loadFile("src/test/resources/mainTest01.txt");
        Collection<Payment> payments = paymentStore.getAll();
        assertEquals(3, payments.size());
        assertTrue(payments.contains(new Payment(USD, 900)));
        assertTrue(payments.contains(new Payment(RMB, 2000)));
        assertTrue(payments.contains(new Payment(HKD, 300)));
    }
}
