package cz.cincibus.bschomework;

import cz.cincibus.bschomework.data.ExchangeRateStore;
import cz.cincibus.bschomework.model.ExchangeRate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Petr Cincibus on 08.12.15.
 * email: petrcincibus@gmail.com
 */
public class ExchangeRateStoreTest {

    private  static final String CZK = "CZK";

    private ExchangeRateStore store = ExchangeRateStore.getInstance();

    @Before
    public void initialize(){
        store.clear();
    }

    @Test
    public void simpleInsert(){
        ExchangeRate rate = new ExchangeRate(CZK,100);
        store.add(rate);
        assertEquals(rate, store.get(rate.name));
    }

    @Test
    public void doubleInsert(){
        ExchangeRate rate1 = new ExchangeRate(CZK, 100);
        ExchangeRate rate2 = new ExchangeRate(CZK, 200);
        store.add(rate1);
        store.add(rate2);
        assertEquals(rate2, store.get(CZK));
    }
}
