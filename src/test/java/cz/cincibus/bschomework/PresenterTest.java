package cz.cincibus.bschomework;

import cz.cincibus.bschomework.data.ExchangeRateStore;
import cz.cincibus.bschomework.data.PaymentStore;
import cz.cincibus.bschomework.model.ExchangeRate;
import cz.cincibus.bschomework.model.Payment;
import cz.cincibus.bschomework.model.PaymentWithRate;
import cz.cincibus.bschomework.presentation.InputHelper;
import cz.cincibus.bschomework.presentation.Presenter;
import cz.cincibus.bschomework.presentation.PrintTimer;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Petr Cincibus on 03.12.15.
 * email: petrcincibus@gmail.com
 */
public class PresenterTest {

    private static final String CZK = "CZK";

    private static final String EUR = "EUR";

    private TestStatus status;

    private String result;

    private Presenter presenter;

    @Before
    public void initialize(){
        status = TestStatus.STARTING;
        PaymentStore.getInstance().clear();
        ExchangeRateStore.getInstance().clear();
        PrintTimer.interval = 1;
        TestView view = new TestView(this::testResult);
        presenter = new Presenter(view);
        presenter.initialize();
    }

    @Test
    public void testAddOnePayment() throws InterruptedException {
        Payment payment1 = new Payment(CZK, 100);
        presenter.insertedLine(inputLine(payment1));
        waitForResult();
        assertEquals(inputLine(payment1), result);
    }

    @Test
    public void testPaymentWithRate() {
        Payment payment1 = new Payment(CZK, 100);
        ExchangeRate exchangeRate1 = new ExchangeRate(CZK, 25.55);
        PaymentWithRate paymentWithRate = new PaymentWithRate(payment1, exchangeRate1);
        presenter.insertedLine(inputLine(payment1));
        presenter.insertedLine(inputLine(exchangeRate1));
        waitForResult();
        assertEquals(paymentWithRate.toString(), result);
    }

    /**
     * Method waiting for results printed to the view.
     */
    private synchronized void waitForResult() {
        status = TestStatus.WAITING_FOR_RESULT;
        while (status != TestStatus.COMPLETE) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method which saves result of printing to the view.
     * @param result printed text to the view
     */
    public synchronized void testResult(String result){
        if(status != TestStatus.WAITING_FOR_RESULT) return;
        this.result = result;
        status = TestStatus.COMPLETE;
        notifyAll();
    }

    /**
     * Creates string representation of payment corresponding to input command.
     * @param payment object representing input command
     * @return string representation of the command
     */
    private String inputLine(Payment payment){
        StringBuilder sb = new StringBuilder();
        sb.append(payment.name);
        sb.append(" ");
        sb.append(payment.value);
        return sb.toString();
    }

    /**
     * Creates string representation of payment corresponding to input command.
     * @param exchangeRate object representing input command
     * @return string representation of the command
     */
    private String inputLine(ExchangeRate exchangeRate){
        StringBuilder sb = new StringBuilder();
        sb.append(InputHelper.EXCHANGE_RATE_KEYWORD);
        sb.append(" ");
        sb.append(exchangeRate.name);
        sb.append(" ");
        sb.append(exchangeRate.value);
        return sb.toString();
    }

    private enum TestStatus{
        COMPLETE, STARTING, WAITING_FOR_RESULT
    }
}
