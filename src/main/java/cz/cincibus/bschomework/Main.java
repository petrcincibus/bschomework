package cz.cincibus.bschomework;

import cz.cincibus.bschomework.presentation.ConsoleView;
import cz.cincibus.bschomework.presentation.InputHelper;
import cz.cincibus.bschomework.presentation.Presenter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Petr Cincibus on 06.12.15.
 * email: petrcincibus@gmail.com
 */
public class Main {

    public static void main(String[] args){
        if(args.length > 0) loadFile(args[0]);
        initializePresentation();
    }

    private static void initializePresentation(){
        ConsoleView view = new ConsoleView();
        Presenter presenter = new Presenter(view);
        view.setPresenter(presenter);
        presenter.initialize();
        view.readInput();
    }

    /**
     * Loads input from file.
     * @param path path to the input file
     */
    public static void loadFile(String path) {
        InputHelper helper = new InputHelper();
        try (BufferedReader br = new BufferedReader(new FileReader(path))){
            String line;
            while((line = br.readLine()) != null){
                helper.loadLine(line);
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Input file was not found.");
            System.exit(0);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}