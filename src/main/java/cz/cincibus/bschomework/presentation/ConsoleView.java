package cz.cincibus.bschomework.presentation;

import java.util.Scanner;

/**
 * Created by Petr Cincibus on 05.12.15.
 * email: petrcincibus@gmail.com
 */
public class ConsoleView implements View {

    private Presenter presenter;

    @Override
    public void print(String text) {
        System.out.println(text);
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void readInput() {
        Scanner scanner = new Scanner(System.in);
        if (scanner == null) {
            System.out.println("Unable to fetch console");
            return;
        }
        while(true){
            presenter.insertedLine(scanner.nextLine());
        }
    }
}
