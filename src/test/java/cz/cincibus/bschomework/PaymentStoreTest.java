package cz.cincibus.bschomework;

import cz.cincibus.bschomework.data.PaymentStore;
import cz.cincibus.bschomework.model.Payment;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by Petr Cincibus on 04.12.15.
 * email: petrcincibus@gmail.com
 */
public class PaymentStoreTest {

    private static final String USD = "USD";

    private static final String CZK = "CZK";

    private PaymentStore store = PaymentStore.getInstance();

    @Before
    public void initialize(){
        store.clear();
    }

    @Test
    public void testSimpleAdd(){
        Payment payment = new Payment(USD, 11);
        store.add(payment);
        Collection<Payment> collection = store.getAll();
        assertEquals(collection.size(), 1);
        assertTrue(collection.contains(payment));
    }

    @Test
    public void testAddSum(){
        Payment payment1 = new Payment(USD, 11);
        Payment payment2 = new Payment(USD, 5);
        store.add(payment1);
        store.add(payment2);
        Collection<Payment> payments = store.getAll();
        assertEquals(1, payments.size());
        assertTrue(payments.contains(new Payment(USD, 16)));
    }

    @Test
    public void testAddNegative(){
        Payment payment1 = new Payment(USD, -11);
        Payment payment2 = new Payment(USD, -5);
        store.add(payment1);
        store.add(payment2);
        Collection<Payment> payments = store.getAll();
        assertEquals(1, payments.size());
        assertTrue(payments.contains(new Payment(USD, -16)));
    }

    @Test
    public void testAddDifferentCurrencies(){
        Payment payment1 = new Payment(USD, -11);
        Payment payment2 = new Payment(CZK, -5);
        store.add(payment1);
        store.add(payment2);
        Collection<Payment> payments = store.getAll();
        assertEquals(2, payments.size());
        assertTrue(payments.contains(new Payment(USD, -11)));
        assertTrue(payments.contains(new Payment(CZK, -5)));
    }
}
