package cz.cincibus.bschomework.domain;

import cz.cincibus.bschomework.data.ExchangeRateStore;
import cz.cincibus.bschomework.data.PaymentStore;
import cz.cincibus.bschomework.model.ExchangeRate;
import cz.cincibus.bschomework.model.Payment;
import cz.cincibus.bschomework.model.PaymentWithRate;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Petr Cincibus on 05.12.15.
 * email: petrcincibus@gmail.com
 */
public class PaymentService {


    public void add(Payment payment) {
        PaymentStore.getInstance().add(payment);
    }

    /**
     * Function returns all payments with exchange rate if it is presented in the exchange rate store. If exchange rate
     * is not presented in the store then rate is set to null in PaymentWithRate object.
     * @return collection of payments with exchange rate
     */
    public Collection<PaymentWithRate> getAllPaymentWithRate(){
        Collection<Payment> payments = PaymentStore.getInstance().getAll();
        ExchangeRateStore exchangeRateStore = ExchangeRateStore.getInstance();
        Collection<PaymentWithRate> result = new ArrayList<>();
        for(Payment payment:payments){
            ExchangeRate exchangeRate = exchangeRateStore.get(payment.name);
            result.add(new PaymentWithRate(payment, exchangeRate));
        }
        return result;
    }

}
