package cz.cincibus.bschomework;

import org.junit.Before;
import org.junit.Test;
import cz.cincibus.bschomework.presentation.InputHelper;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Petr Cincibus on 08.12.15.
 * email: petrcincibus@gmail.com
 */
public class InputHelperTest {

    InputHelper helper;

    @Before
    public void before(){
        helper = new InputHelper();
    }

    @Test
    public void testIsLegalSimple(){
        assertTrue(helper.isLegal("CZK 11"));
    }

    @Test
    public void testIsLegalShortCurrency(){
        assertFalse(helper.isLegal("CZ 10"));
    }

    @Test
    public void testIsLegalNoNumber(){
        assertFalse(helper.isLegal("CZK "));
    }

    @Test
    public void testIsLegalNegative(){
        assertTrue(helper.isLegal("CZK -10"));
    }

    @Test
    public void testIsLegalDotDouble(){
        assertFalse(helper.isLegal("EUR 1.1"));
    }

    @Test
    public void testIsLegalCommaDouble(){
        assertFalse(helper.isLegal("EUR 1,1"));
    }

    @Test
    public void testIsLegalPaymentLongerCurrency(){
        assertFalse(helper.isLegal("aCZK 1"));
    }

    @Test
    public void testIsLegalStartingTrailingWhitespaces(){
        assertTrue(helper.isLegal(" CZK 11 "));
    }

    @Test
    public void testIsLegalExchangeWithDouble(){
        assertTrue(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD+" CZK 25.55"));
    }

    @Test
    public void testIsLegalExchangeWithInteger(){
        assertTrue(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD+" CZK 25"));
    }

    @Test
    public void testIsLegalExchangeNegative(){
        assertFalse(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD + " CZK -25"));
    }

    @Test
    public void testIsLegalExchangeShortCurrency(){
        assertFalse(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD+" AA 25"));
    }

    @Test
    public void testIsLegalExchangeLongCurrency(){
        assertFalse(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD + " AAAA 25"));
    }

    @Test
    public void testIsLegalExchangeCommaDouble(){
        assertFalse(helper.isLegal(InputHelper.EXCHANGE_RATE_KEYWORD + " CZK 25,55"));
    }

}
