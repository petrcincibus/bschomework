package cz.cincibus.bschomework.model;

/**
 * Created by Petr Cincibus on 04.12.15.
 * email: petrcincibus@gmail.com
 */
public class ExchangeRate{

    /**
     * Name of original exchange currency.
     */
    public String name;

    /**
     * Exchange rate
     */
    public double value;

    public ExchangeRate(String name, double value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExchangeRate that = (ExchangeRate) o;

        if (Double.compare(that.value, value) != 0) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        temp = Double.doubleToLongBits(value);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
