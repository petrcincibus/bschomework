package cz.cincibus.bschomework.presentation;

import cz.cincibus.bschomework.domain.ExchangeRateService;
import cz.cincibus.bschomework.domain.PaymentService;
import cz.cincibus.bschomework.model.ExchangeRate;
import cz.cincibus.bschomework.model.Payment;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Petr Cincibus on 06.12.15.
 * email: petrcincibus@gmail.com
 */
public class InputHelper {

    public static final String EXCHANGE_RATE_KEYWORD = "exchangeRate";

    private static final String REGEX_PAYMENT = "[A-Z]{3} -?\\d+";

    private static final String REGEX_EXCHANGE_RATE = EXCHANGE_RATE_KEYWORD + " [A-Z]{3} \\d+(\\.\\d+)?";

    private static final String REGEX = "\\s*("+ REGEX_PAYMENT + "|" + REGEX_EXCHANGE_RATE + ")\\s*";

    private PaymentService paymentService = new PaymentService();

    private ExchangeRateService exchangeRateService = new ExchangeRateService();

    private Pattern pattern;

    public InputHelper() {
        pattern = Pattern.compile(REGEX);
    }

    /**
     * Parse and execute command.
     * @param line string representing command
     */
    public void loadLine(String line){
        StringTokenizer st = new StringTokenizer(line);
        line = line.trim();
        String first = st.nextToken();
        if(first.equals(EXCHANGE_RATE_KEYWORD)){
            String currency = st.nextToken();
            Double exchangeRate = Double.parseDouble(st.nextToken());
            exchangeRateService.add(new ExchangeRate(currency, exchangeRate));
        } else {
            int value = Integer.parseInt(st.nextToken());
            paymentService.add(new Payment(first, value));
        }
    }

    /**
     * Check if string is valid command.
     * @param line string with command to be checked
     * @return
     */
    public boolean isLegal(String line){
        Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }
}
