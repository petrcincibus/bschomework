The application realizes all homework requirements including optional requirement with exchange rate to USD. Exchange
rates are configured via commands inserted to the console or included in input file. Echange rate commands have following
syntax: exchangeRate $originalCurrencyShortcut exchageRateValue (example: exchangerate CZK 25.55)
If a user command has a wrong syntax then the program prints a warning and program execution continues without an exit.

Compile application and run tests:
mvn test

Run application:
mvn exec:java -Dexec.args="InputFilePath" (example: mvn exec:java -Dexec.args="src/test/resources/mainTest01.txt")

If a expath to the input file is not valid then the program prints message input file was not found and exits. Another
command line parameters are ignored.

Java 1.8 is required

