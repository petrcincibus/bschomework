package cz.cincibus.bschomework.presentation;

/**
 * Created by Petr Cincibus on 03.12.15.
 * email: petrcincibus@gmail.com
 */
public interface View {
    void print(String text);
}
