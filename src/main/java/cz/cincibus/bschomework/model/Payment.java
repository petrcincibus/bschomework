package cz.cincibus.bschomework.model;

/**
 * Created by Petr Cincibus on 03.12.15.
 * email: petrcincibus@gmail.com
 */
public class Payment {

    /**
     * Currency name
     */
    public String name;

    public int value;

    public Payment(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        if (value != payment.value) return false;
        return name.equals(payment.name);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" ");
        sb.append(value);
        return sb.toString();
    }
}
