package cz.cincibus.bschomework;

import cz.cincibus.bschomework.data.ExchangeRateStore;
import cz.cincibus.bschomework.data.PaymentStore;
import cz.cincibus.bschomework.domain.ExchangeRateService;
import cz.cincibus.bschomework.domain.PaymentService;
import cz.cincibus.bschomework.model.ExchangeRate;
import cz.cincibus.bschomework.model.Payment;
import cz.cincibus.bschomework.model.PaymentWithRate;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Petr Cincibus on 06.12.15.
 * email: petrcincibus@gmail.com
 */
public class PaymentServiceTest {

    private static final String USD = "USD";

    private static final String CZK = "CZK";

    private static final String EUR = "EUR";

    private PaymentStore paymentStore = PaymentStore.getInstance();

    private ExchangeRateStore exchangeRateStore = ExchangeRateStore.getInstance();

    private PaymentService paymentService;

    private ExchangeRateService exchangeRateService;

    @Before
    public void initialize(){
        paymentService = new PaymentService();
        exchangeRateService = new ExchangeRateService();
        paymentStore.clear();
        exchangeRateStore.clear();
    }

    @Test
    public void testGetAllPaymentWithRateSinglePaymentWithoutRate(){
        Payment payment1 = new Payment(CZK, 11);
        ExchangeRate rate1 = new ExchangeRate(EUR, 0.9);
        paymentService.add(payment1);
        exchangeRateService.add(rate1);
        Collection<PaymentWithRate> collection = paymentService.getAllPaymentWithRate();
        PaymentWithRate expected = new PaymentWithRate(payment1, null);
        assertEquals(1, collection.size());
        assertTrue(collection.contains(expected));
    }

    @Test
    public void testGetAllPaymentWithRateSinglePaymentWithRate(){
        Payment payment1 = new Payment(CZK, 11);
        ExchangeRate rate1 = new ExchangeRate(CZK, 25.5);
        paymentService.add(payment1);
        exchangeRateService.add(rate1);
        Collection<PaymentWithRate> collection = paymentService.getAllPaymentWithRate();
        PaymentWithRate expected = new PaymentWithRate(payment1, rate1);
        assertEquals(1, collection.size());
        assertTrue(collection.contains(expected));
    }

}
