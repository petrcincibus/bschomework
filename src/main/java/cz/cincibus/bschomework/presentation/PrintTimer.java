package cz.cincibus.bschomework.presentation;

/**
 * Created by Petr Cincibus on 04.12.15.
 * email: petrcincibus@gmail.com
 */
public class PrintTimer extends Thread {

    public static final long DEFAULT_INTERVAL_MILISECONDS = 60000;

    public static long interval = DEFAULT_INTERVAL_MILISECONDS;

    private Presenter presenter;

    public PrintTimer(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(interval);
                presenter.printNotify();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
